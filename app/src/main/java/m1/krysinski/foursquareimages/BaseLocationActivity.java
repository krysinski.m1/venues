package m1.krysinski.foursquareimages;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import m1.krysinski.foursquareimages.permissions.LocationPermissionManager;

import static m1.krysinski.foursquareimages.permissions.model.Permissions.LOCATION;

public abstract class BaseLocationActivity extends AppCompatActivity {

    protected LocationPermissionManager locationPermissionManager;

    protected void setLocationPermissionManager(LocationPermissionManager locationPermissionManager){
        this.locationPermissionManager = locationPermissionManager;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == LOCATION.getRequestCode()){
            locationPermissionManager.handleRequestPermissionResult(grantResults);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
