package m1.krysinski.foursquareimages;

import android.app.Fragment;

import m1.krysinski.foursquareimages.permissions.LocationPermissionManager;

public abstract class BaseLocationFragment extends Fragment implements LocationPermissionManager.Callbacks {

}
