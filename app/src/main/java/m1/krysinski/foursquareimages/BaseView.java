package m1.krysinski.foursquareimages;

public interface BaseView<P extends BasePresenter> {

    void setPresenter(P presenter);

}
