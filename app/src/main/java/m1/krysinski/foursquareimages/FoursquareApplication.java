package m1.krysinski.foursquareimages;

import android.app.Application;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import m1.krysinski.foursquareimages.data.venues.model.FoursquareVenuesService;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FoursquareApplication extends Application {

    private static OkHttpClient httpClient;

    private static FoursquareVenuesService venuesService;

    private static int maxPhotos;

    private static int venueLimit;

    public static FoursquareVenuesService provideVenuesService(){
        return venuesService;
    }

    public static int provideMaxPhotos(){
        return maxPhotos;
    }

    public static int provideVenueLimit(){
        return venueLimit;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        maxPhotos = getResources().getInteger(R.integer.max_photos);
        venueLimit = getResources().getInteger(R.integer.venue_limit);
        Cache cache = new Cache(getCacheDir(), getResources().getInteger(R.integer.cache_size_megabytes) * 1024 * 1024);
        httpClient = new OkHttpClient.Builder().cache(cache)
                .addInterceptor(new FoursquareAPIInterceptor(
                        getString(R.string.foursquare_client_id),
                        getString(R.string.foursquare_client_secret),
                        getString(R.string.foursquare_api_version))
                ).build();
        Retrofit retrofit = new Retrofit.Builder().client(httpClient).baseUrl(getString(R.string.base_url)).addConverterFactory(GsonConverterFactory.create()).build();
        venuesService = retrofit.create(FoursquareVenuesService.class);
        Picasso picasso = new Picasso.Builder(this).downloader(new OkHttp3Downloader(httpClient)).build();
        Picasso.setSingletonInstance(picasso);
        Picasso.with(this).setIndicatorsEnabled(true);
        Picasso.with(this).setLoggingEnabled(true);
    }

    private static class FoursquareAPIInterceptor implements Interceptor{

        private static final String Q_CLIENT_ID = "client_id";

        private static final String Q_CLIENT_SECRET = "client_secret";

        private static final String Q_VERSION = "v";

        private final String clientId;

        private final String clientSecret;

        private final String apiVersion;

        FoursquareAPIInterceptor(String clientId, String clientSecret, String apiVersion){
            this.clientId = clientId;
            this.clientSecret = clientSecret;
            this.apiVersion = apiVersion;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            HttpUrl url = request.url();
            url = url.newBuilder()
                    .addQueryParameter(Q_CLIENT_ID, clientId)
                    .addQueryParameter(Q_CLIENT_SECRET, clientSecret)
                    .addQueryParameter(Q_VERSION,apiVersion)
                    .build();
            request = request.newBuilder().url(url).build();
            return chain.proceed(request);
        }
    }

}
