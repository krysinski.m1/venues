package m1.krysinski.foursquareimages.data.location.converter;

import java.util.Locale;

import m1.krysinski.foursquareimages.data.location.model.DeviceLocation;

/**
 * Created by mich on 05/03/2017.
 */

public class DefaultLocationConverter implements LocationConverter {

    private static final String FORMAT = "%.02f,%.02f";

    @Override
    public String convert(DeviceLocation location) {
        String res = "";
        if(location != null){
            res = convert(location.getLatitude(), location.getLongitude());
        }
        return res;
    }

    @Override
    public String convert(double latitude, double longitude) {
        return String.format(Locale.ENGLISH, FORMAT, latitude, longitude);
    }

}
