package m1.krysinski.foursquareimages.data.location.converter;

import m1.krysinski.foursquareimages.data.location.model.DeviceLocation;

/**
 * Created by mich on 05/03/2017.
 */

public interface LocationConverter {

    String convert(DeviceLocation location);

    String convert(double latitude, double longitude);

}
