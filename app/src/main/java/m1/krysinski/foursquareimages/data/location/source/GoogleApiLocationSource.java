package m1.krysinski.foursquareimages.data.location.source;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import m1.krysinski.foursquareimages.data.location.model.DeviceLocation;

public class GoogleApiLocationSource implements LocationSource, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private GoogleApiClient googleApiClient;

    private Callback callback;

    private boolean locationRequested = false;

    public GoogleApiLocationSource(AppCompatActivity activity){
        googleApiClient = new GoogleApiClient.Builder(activity).enableAutoManage(activity, this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void getCurrentLocation() {
        if(googleApiClient.isConnected()){
            getLocationAndNotify();
        }else{
            locationRequested = true;
        }
    }

    @Override
    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    private void getLocationAndNotify(){
        DeviceLocation location = getLocation();
        if(location != null){
            callback.onLocationAcquired(location);
        }else{
            callback.onLocationError();
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private DeviceLocation getLocation(){
        Location location =  LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if(location != null){
            return new DeviceLocation(location.getLatitude(), location.getLongitude());
        }
        return null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(locationRequested){
            locationRequested = false;
            getLocationAndNotify();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //no op
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        callback.onLocationError();
    }

}
