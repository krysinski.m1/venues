package m1.krysinski.foursquareimages.data.location.source;

import m1.krysinski.foursquareimages.data.location.model.DeviceLocation;

public interface LocationSource {

    void getCurrentLocation();

    void setCallback(Callback callback);

    interface Callback {

        void onLocationAcquired(DeviceLocation location);

        void onLocationError();

    }

}
