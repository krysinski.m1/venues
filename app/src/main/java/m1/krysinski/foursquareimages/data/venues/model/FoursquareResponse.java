package m1.krysinski.foursquareimages.data.venues.model;

import com.google.gson.annotations.Expose;

import java.util.List;

public class FoursquareResponse {

    @Expose
    private Meta meta;

    private Meta getMeta(){
        return meta;
    }

    public static class Meta{

        @Expose
        private int status;

        @Expose
        private String requestId;

        public int getStatus(){
            return status;
        }

        public String getRequestId(){
            return requestId;
        }

    }

}
