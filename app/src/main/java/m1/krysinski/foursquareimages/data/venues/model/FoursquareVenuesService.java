package m1.krysinski.foursquareimages.data.venues.model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FoursquareVenuesService {

    @GET("venues/search")
    Call<VenuesResponse> searchVenues(@Query("ll") String at, @Query("limit") int limit);

    @GET("venues/{venueId}/photos")
    Call<PhotosResponse> venuePhotos(@Path("venueId") String venueId);

}
