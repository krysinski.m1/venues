package m1.krysinski.foursquareimages.data.venues.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;
import com.google.gson.annotations.Expose;

public class Photo implements Parcelable{

    @Expose
    private String id;

    @Expose
    private Long createdAt;

    @Expose
    private String prefix;

    @Expose
    private String suffix;

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };


    public Photo(){
        //GSON
    }

    protected Photo(Parcel in) {
        id = in.readString();
        createdAt = in.readLong();
        prefix = in.readString();
        suffix = in.readString();
    }

    public String getId(){
        return id;
    }

    public Long getCreatedAt(){
        return createdAt;
    }

    public String getPrefix(){
        return prefix;
    }

    public String getSuffix(){
        return suffix;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, createdAt, prefix, suffix);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof Photo)){
            return false;
        }
        Photo other = (Photo) obj;
        return Objects.equal(id, other.id)
                && Objects.equal(createdAt, other.createdAt)
                && Objects.equal(prefix, other.prefix)
                && Objects.equal(suffix, other.suffix);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeLong(createdAt);
        parcel.writeString(prefix);
        parcel.writeString(suffix);
    }
}
