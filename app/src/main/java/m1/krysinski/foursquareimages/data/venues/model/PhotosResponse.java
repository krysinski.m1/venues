package m1.krysinski.foursquareimages.data.venues.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

public class PhotosResponse extends FoursquareResponse{

    @Expose
    @SerializedName("response")
    private Body body;

    public Body getBody(){
        return body;
    }

    public static class Photos{

        @Expose
        private int count;

        @Expose
        private List<Photo> items;

        public int getCount(){
            return count;
        }

        public List<Photo> getItems(){
            if(items == null){
                return Collections.emptyList();
            }
            return Collections.unmodifiableList(items);
        }

    }

    public static class Body{

        @Expose
        private Photos photos;

        public Photos getPhotos(){
            return photos;
        }

    }

}
