package m1.krysinski.foursquareimages.data.venues.model;

import com.google.gson.annotations.Expose;

/**
 * Created by mich on 05/03/2017.
 */

public class Venue {

    @Expose
    private String id;

    @Expose
    private String name;

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

}
