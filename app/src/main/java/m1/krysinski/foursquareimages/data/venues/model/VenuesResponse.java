package m1.krysinski.foursquareimages.data.venues.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

public class VenuesResponse extends FoursquareResponse {

    @Expose
    @SerializedName("response")
    private Body body;

    public Body getBody(){
        return body;
    }

    public static class Body{

        @Expose
        private List<Venue> venues;

        public List<Venue> getVenues(){
            if(venues == null){
                return Collections.emptyList();
            }
            return Collections.unmodifiableList(venues);
        }

    }

}
