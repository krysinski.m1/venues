package m1.krysinski.foursquareimages.data.venues.source;

import java.util.List;

import m1.krysinski.foursquareimages.data.venues.model.Photo;

public interface PhotosSource {

    interface GetPhotosCallback{

        void onPhotosLoaded(List<Photo> photos);

        void onError();

    }

    void getPhotos(double latitude, double longitude, GetPhotosCallback callback);

}
