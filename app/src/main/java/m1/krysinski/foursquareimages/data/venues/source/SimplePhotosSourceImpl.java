package m1.krysinski.foursquareimages.data.venues.source;

import android.os.Handler;
import android.os.Looper;

import com.google.common.collect.ComparisonChain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import m1.krysinski.foursquareimages.data.venues.model.FoursquareVenuesService;
import m1.krysinski.foursquareimages.data.venues.model.Photo;
import m1.krysinski.foursquareimages.data.venues.model.PhotosResponse;
import m1.krysinski.foursquareimages.data.venues.model.Venue;
import m1.krysinski.foursquareimages.data.venues.model.VenuesResponse;
import m1.krysinski.foursquareimages.data.location.converter.LocationConverter;
import retrofit2.Response;

public class SimplePhotosSourceImpl implements PhotosSource {

    private static final Comparator<Photo> PHOTO_COMPARATOR = new Comparator<Photo>() {
        @Override
        public int compare(Photo photo, Photo t1) {
            return ComparisonChain.start()
                    .compare(photo.getCreatedAt(), t1.getCreatedAt())
                    .compare(photo.getId(), t1.getId()).result();
        }
    };

    private FoursquareVenuesService service;

    private LocationConverter converter;

    private final int maxPhotos;

    private final int venueLimit;

    private ExecutorService executorService;

    public SimplePhotosSourceImpl(FoursquareVenuesService service, LocationConverter converter, int maxPhotos, int venueLimit) {
        this.service = service;
        this.converter = converter;
        this.maxPhotos = maxPhotos;
        this.venueLimit = venueLimit;
        executorService = Executors.newSingleThreadExecutor();
    }

    @Override
    public void getPhotos(double latitude, double longitude, final GetPhotosCallback callback) {
        final String coordinates = converter.convert(latitude, longitude);
        final SortedSet<Photo> resultSet = new TreeSet<>(PHOTO_COMPARATOR);
        final Handler handler = new Handler(Looper.getMainLooper());
        executorService.submit(new Runnable(){
            @Override
            public void run() {
                ServiceResponse<List<Venue>> venuesResponse = searchVenues(coordinates);
                if(!venuesResponse.error){
                    Iterator<Venue> iter = venuesResponse.value.iterator();
                    while(iter.hasNext() && resultSet.size() < maxPhotos){
                        Venue v = iter.next();
                        List<Photo> photosResponse = getPhotos(v);
                        resultSet.addAll(photosResponse);
                    }
                    List<Photo> results = new ArrayList<>();
                    results.addAll(resultSet);
                    if(resultSet.size() > maxPhotos){
                        results = results.subList(0, maxPhotos);
                    }
                    callbackSuccess(handler, results, callback);
                }else{
                    callbackError(handler, callback);
                }
            }
        });
    }

    private ServiceResponse<List<Venue>> searchVenues(String coordinates){
        ServiceResponse<List<Venue>> res;
        try {
            Response<VenuesResponse> response = service.searchVenues(coordinates, venueLimit).execute();
            if(response.isSuccessful()){
                res = new ServiceResponse<>(response.body().getBody().getVenues());
            }else{
                res = new ServiceResponse<>();
            }
        } catch (IOException e) {
            res = new ServiceResponse<>();
        }
        return res;
    }

    private List<Photo> getPhotos(Venue venue){
        List<Photo> res = Collections.emptyList();
        try {
            Response<PhotosResponse> response = service.venuePhotos(venue.getId()).execute();
            if(response.isSuccessful()){
                res = response.body().getBody().getPhotos().getItems();
            }
        } catch (IOException e) {
            // no-op
        }
        return res;
    }

    private void callbackSuccess(Handler handler, final List<Photo> photos, final GetPhotosCallback callback){
        handler.post(new Runnable() {
            @Override
            public void run() {
                callback.onPhotosLoaded(photos);
            }
        });
    }

    private void callbackError(Handler handler, final GetPhotosCallback callback){
        handler.post(new Runnable() {
            @Override
            public void run() {
                callback.onError();
            }
        });
    }

    private static class ServiceResponse<T>{

        private final T value;

        private final boolean error;

        ServiceResponse(T value){
            this.value = value;
            this.error = false;
        }

        ServiceResponse(){
            this.value = null;
            this.error = true;
        }

    }

}
