package m1.krysinski.foursquareimages.location;

import android.os.Bundle;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import m1.krysinski.foursquareimages.BaseLocationActivity;
import m1.krysinski.foursquareimages.R;
import m1.krysinski.foursquareimages.data.location.source.GoogleApiLocationSource;
import m1.krysinski.foursquareimages.data.location.converter.DefaultLocationConverter;
import m1.krysinski.foursquareimages.permissions.LocationPermissionManagerImpl;
import m1.krysinski.foursquareimages.permissions.helper.PermissionsHelperFactoryImpl;

public class LocationActivity extends BaseLocationActivity {

    @BindView(R.id.am_container)
    View mainContainer;

    private LocationFragment locationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //TODO injection
        locationFragment = (LocationFragment) getFragmentManager().findFragmentById(R.id.am_fragment_container);
        if(locationFragment == null){
            locationFragment = LocationFragment.newInstance();
            getFragmentManager().beginTransaction().add(R.id.am_fragment_container, locationFragment).commit();
        }

        setLocationPermissionManager(new LocationPermissionManagerImpl(new PermissionsHelperFactoryImpl(this).getPermissionsHelper(), locationFragment));

        LocationContract.Presenter locationPresenter = new LocationPresenter(new GoogleApiLocationSource(this), locationPermissionManager, new DefaultLocationConverter(), locationFragment);

        ButterKnife.bind(this);
    }

}
