package m1.krysinski.foursquareimages.location;

import m1.krysinski.foursquareimages.BasePresenter;
import m1.krysinski.foursquareimages.BaseView;

public interface LocationContract {

    interface View extends BaseView<Presenter>{

        void showLocation(String location);

        void showLocationError();

    }

    interface Presenter extends BasePresenter{

        void loadLocation();

        void requestPermission();

    }

}
