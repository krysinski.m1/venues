package m1.krysinski.foursquareimages.location;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import m1.krysinski.foursquareimages.BaseLocationFragment;
import m1.krysinski.foursquareimages.R;

public class LocationFragment extends BaseLocationFragment implements LocationContract.View{

    @BindView(R.id.fl_container)
    View mainContainer;

    @BindView(R.id.fl_location)
    TextView locationDisplay;

    private LocationContract.Presenter presenter;

    private Unbinder unbinder;

    public static LocationFragment newInstance(){
        return new LocationFragment();
    }

    public LocationFragment(){}


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View res = inflater.inflate(R.layout.fragment_location, container, false);
        unbinder = ButterKnife.bind(this, res);
        return res;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void locationPermissionAccepted() {
        presenter.loadLocation();
    }

    @Override
    public void locationPermissionDenied() {
        Snackbar.make(mainContainer, R.string.message_location_permission_denied, Snackbar.LENGTH_INDEFINITE).setAction(R.string.label_ok, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeApp();
            }
        }).show();
    }

    private void closeApp(){
        Activity activity = getActivity();
        if(activity != null){
            activity.finish();
        }
    }

    @Override
    public void showLocationRationale() {
        Snackbar.make(mainContainer, R.string.message_location_rationale, Snackbar.LENGTH_INDEFINITE).setAction(R.string.label_ok, new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                presenter.requestPermission();
            }

        }).show();
    }

    @Override
    public void setPresenter(LocationContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showLocation(String location) {
        locationDisplay.setText(location);
    }

    @Override
    public void showLocationError() {
        Snackbar.make(mainContainer, R.string.message_location_error, Snackbar.LENGTH_LONG).show();
    }
}
