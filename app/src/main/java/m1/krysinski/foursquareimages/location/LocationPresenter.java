package m1.krysinski.foursquareimages.location;

import m1.krysinski.foursquareimages.data.location.source.LocationSource;
import m1.krysinski.foursquareimages.data.location.converter.LocationConverter;
import m1.krysinski.foursquareimages.data.location.model.DeviceLocation;
import m1.krysinski.foursquareimages.permissions.LocationPermissionManager;

public class LocationPresenter implements LocationContract.Presenter, LocationSource.Callback {

    private LocationSource locationSource;

    private LocationPermissionManager locationPermissionManager;

    private LocationConverter locationConverter;

    private LocationContract.View view;

    public LocationPresenter(LocationSource locationSource, LocationPermissionManager locationPermissionManager, LocationConverter locationConverter, LocationContract.View view){
        this.locationSource = locationSource;
        this.locationSource.setCallback(this);
        this.locationPermissionManager = locationPermissionManager;
        this.locationConverter = locationConverter;
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void start() {
        loadLocation();
    }

    @Override
    public void loadLocation() {
        if(locationPermissionManager.hasLocationPermission()){
            locationSource.getCurrentLocation();
        }else{
            locationPermissionManager.requestLocationPermission();
        }
    }

    @Override
    public void requestPermission() {
        locationPermissionManager.requestLocationPermission();
    }

    @Override
    public void onLocationAcquired(DeviceLocation location) {
        String convertedLocation = locationConverter.convert(location);
        view.showLocation(convertedLocation);
    }

    @Override
    public void onLocationError() {
        view.showLocationError();
    }

}
