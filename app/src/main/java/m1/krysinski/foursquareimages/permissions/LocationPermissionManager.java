package m1.krysinski.foursquareimages.permissions;

public interface LocationPermissionManager {

    void requestLocationPermission();

    boolean hasLocationPermission();

    void handleRequestPermissionResult(int[] grantResults);

    interface Callbacks{

        void locationPermissionAccepted();

        void locationPermissionDenied();

        void showLocationRationale();
    }

}
