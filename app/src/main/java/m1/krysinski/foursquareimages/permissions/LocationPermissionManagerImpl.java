package m1.krysinski.foursquareimages.permissions;

import m1.krysinski.foursquareimages.permissions.helper.PermissionsHelper;
import m1.krysinski.foursquareimages.permissions.util.PermissionsVerifier;
import static m1.krysinski.foursquareimages.permissions.model.Permissions.*;

public class LocationPermissionManagerImpl implements LocationPermissionManager {

    private PermissionsHelper permissionsHandler;

    private PermissionsVerifier permissionsVerifier;

    private Callbacks callbacks;

    public LocationPermissionManagerImpl(PermissionsHelper permissionsHelper, LocationPermissionManager.Callbacks callbacks){
        this.permissionsHandler = permissionsHelper;
        this.callbacks = callbacks;
        this.permissionsVerifier = new PermissionsVerifier();
    }

    @Override
    public void requestLocationPermission() {
        if(permissionsHandler.hasSelfPermission(LOCATION.getPermission())){
            callbacks.locationPermissionAccepted();
        }else{
            if(permissionsHandler.shouldShowRequestPermissionRationale(LOCATION.getPermission())){
                callbacks.showLocationRationale();
            }else{
                permissionsHandler.requestPermissions(new String[]{LOCATION.getPermission()}, LOCATION.getRequestCode());
            }
        }
    }

    @Override
    public boolean hasLocationPermission() {
        return permissionsHandler.hasSelfPermission(LOCATION.getPermission());
    }

    @Override
    public void handleRequestPermissionResult(int[] grantResults) {
        if(permissionsVerifier.verifyPermissions(grantResults)){
            callbacks.locationPermissionAccepted();
        }else{
            callbacks.locationPermissionDenied();
        }
    }

}
