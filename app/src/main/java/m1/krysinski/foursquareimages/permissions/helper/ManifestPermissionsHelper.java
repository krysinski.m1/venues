package m1.krysinski.foursquareimages.permissions.helper;

public class ManifestPermissionsHelper implements PermissionsHelper {

    @Override
    public boolean hasSelfPermission(String permission) {
        return true;
    }

    @Override
    public void requestPermissions(String[] permissions, int requestCode) {
        //no op
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(String permission) {
        return false;
    }

}
