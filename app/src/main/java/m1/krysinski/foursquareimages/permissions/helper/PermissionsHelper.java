package m1.krysinski.foursquareimages.permissions.helper;

public interface PermissionsHelper {

    boolean hasSelfPermission(String permission);

    void requestPermissions(String[] permissions, int requestCode);

    boolean shouldShowRequestPermissionRationale(String permission);

}
