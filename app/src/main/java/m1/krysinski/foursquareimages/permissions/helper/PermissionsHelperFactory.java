package m1.krysinski.foursquareimages.permissions.helper;

public interface PermissionsHelperFactory {

    PermissionsHelper getPermissionsHelper();

}
