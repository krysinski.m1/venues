package m1.krysinski.foursquareimages.permissions.helper;

import android.app.Activity;

import m1.krysinski.foursquareimages.permissions.util.PermissionsUtil;

public class PermissionsHelperFactoryImpl implements PermissionsHelperFactory {

    private Activity activity;

    public PermissionsHelperFactoryImpl(Activity activity){
        this.activity = activity;
    }

    public PermissionsHelper getPermissionsHelper(){
        if (PermissionsUtil.useRuntimePermissions()){
            return new RuntimePermissionsHelper(activity);
        }else{
            return new ManifestPermissionsHelper();
        }
    }

}
