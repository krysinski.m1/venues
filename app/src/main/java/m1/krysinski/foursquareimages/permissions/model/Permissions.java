package m1.krysinski.foursquareimages.permissions.model;

import android.Manifest;

public enum Permissions {

    LOCATION(Manifest.permission.ACCESS_FINE_LOCATION, 42);

    private String permission;

    private int requestCode;

    Permissions(String permission, int requestCode){
        this.permission = permission;
        this.requestCode = requestCode;
    }

    public String getPermission(){
        return permission;
    }

    public int getRequestCode(){
        return requestCode;
    }

}
