package m1.krysinski.foursquareimages.permissions.util;

import android.os.Build;

public final class PermissionsUtil {

    private PermissionsUtil(){}

    public static boolean useRuntimePermissions(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

}
