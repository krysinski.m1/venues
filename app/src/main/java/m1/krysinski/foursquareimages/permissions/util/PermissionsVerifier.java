package m1.krysinski.foursquareimages.permissions.util;

import android.content.pm.PackageManager;

public class PermissionsVerifier {

    public boolean verifyPermissions(int... grantResults){
        if(grantResults.length < 1){
            return false;
        }
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

}
