package m1.krysinski.foursquareimages.photos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import m1.krysinski.foursquareimages.BaseLocationActivity;
import m1.krysinski.foursquareimages.FoursquareApplication;
import m1.krysinski.foursquareimages.R;
import m1.krysinski.foursquareimages.data.location.source.GoogleApiLocationSource;
import m1.krysinski.foursquareimages.data.venues.source.SimplePhotosSourceImpl;
import m1.krysinski.foursquareimages.data.location.converter.DefaultLocationConverter;
import m1.krysinski.foursquareimages.permissions.LocationPermissionManagerImpl;
import m1.krysinski.foursquareimages.permissions.helper.PermissionsHelperFactoryImpl;

public class PhotosActivity extends BaseLocationActivity{

    @BindView(R.id.ap_container)
    View mainContainer;

    private PhotosFragment photosFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        photosFragment = (PhotosFragment) getFragmentManager().findFragmentById(R.id.ap_fragment_container);
        if(photosFragment == null){
            photosFragment = PhotosFragment.getInstance();
            getFragmentManager().beginTransaction().add(R.id.ap_fragment_container, photosFragment).commit();
        }

        setLocationPermissionManager(new LocationPermissionManagerImpl(new PermissionsHelperFactoryImpl(this).getPermissionsHelper(), photosFragment));

        new PhotosPresenter(new GoogleApiLocationSource(this), locationPermissionManager,
                new SimplePhotosSourceImpl(
                        FoursquareApplication.provideVenuesService(),
                        new DefaultLocationConverter(),
                        FoursquareApplication.provideMaxPhotos(),
                        FoursquareApplication.provideVenueLimit()),
                photosFragment);

        ButterKnife.bind(this);
    }

}
