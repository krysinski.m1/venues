package m1.krysinski.foursquareimages.photos;

import java.util.List;

import m1.krysinski.foursquareimages.BasePresenter;
import m1.krysinski.foursquareimages.BaseView;
import m1.krysinski.foursquareimages.data.venues.model.Photo;

public interface PhotosContract {

    interface View extends BaseView<Presenter>{

        void showPhotos(List<Photo> photos);

        void showPhotosError();

        void showLocationError();

        void showProgress();

        void hideProgress();

        void showPhoto(Photo photo);

    }

    interface Presenter extends BasePresenter{

        void loadPhotos();

        void requestPermission();

        void openPhoto(Photo photo);

    }

}
