package m1.krysinski.foursquareimages.photos;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import m1.krysinski.foursquareimages.BaseLocationFragment;
import m1.krysinski.foursquareimages.R;
import m1.krysinski.foursquareimages.data.venues.model.Photo;

public class PhotosFragment extends BaseLocationFragment implements PhotosContract.View{

    @BindView(R.id.fp_container)
    View mainContainer;

    @BindView(R.id.fp_recycler)
    RecyclerView recyclerView;

    PhotosAdapter adapter;

    private ProgressDialog progressDialog;

    private PhotosContract.Presenter presenter;

    private Unbinder unbinder;

    private int imageSize;

    private String imageSizeUri;

    public static PhotosFragment getInstance(){
        return new PhotosFragment();
    }

    public PhotosFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View res = inflater.inflate(R.layout.fragment_photos, container, false);
        unbinder = ButterKnife.bind(this, res);
        imageSize = getResources().getInteger(R.integer.photo_size);
        imageSizeUri = imageSize+"x"+imageSize;
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),getResources().getInteger(R.integer.recycler_columns)));
        recyclerView.setHasFixedSize(true);
        adapter = new PhotosAdapter(imageSize, imageSizeUri, new PhotoClickCallback() {
            @Override
            public void onPhotoClicked(Photo photo) {
                presenter.openPhoto(photo);
            }
        });
        recyclerView.setAdapter(adapter);
        return res;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        removePhotoDialog();
    }

    private void removePhotoDialog(){
        Fragment fragment = getFragmentManager().findFragmentByTag(PhotoDialog.TAG);
        if(fragment != null){
            getFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        hideProgressIfExists();
        super.onDestroyView();
    }

    @Override
    public void setPresenter(PhotosContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void locationPermissionAccepted() {
        presenter.loadPhotos();
    }

    @Override
    public void locationPermissionDenied() {
        Snackbar.make(mainContainer, R.string.message_location_permission_denied, Snackbar.LENGTH_INDEFINITE).setAction(R.string.label_ok, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeApp();
            }
        });
    }

    private void closeApp(){
        Activity activity = getActivity();
        if(activity != null){
            activity.finish();
        }
    }

    @Override
    public void showLocationRationale() {
        Snackbar.make(mainContainer, R.string.message_location_rationale, Snackbar.LENGTH_INDEFINITE).setAction(R.string.label_ok, new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                presenter.requestPermission();
            }

        }).show();
    }

    @Override
    public void showProgress() {
        Activity activity = getActivity();
        if(activity != null && progressDialog == null){
            //progressDialog = ProgressDialog.show(activity, "Loading", "Loading venue photos.", true);
            progressDialog = new ProgressDialog(activity);
            progressDialog.setIndeterminate(true);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setTitle(getString(R.string.title_photos_progress_dialog));
            progressDialog.setMessage(getString(R.string.message_photos_progress_dialog));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        hideProgressIfExists();
    }

    private void hideProgressIfExists(){
        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void showPhotos(List<Photo> photos) {
        adapter.setData(photos);
    }

    @Override
    public void showPhotosError() {
        Snackbar.make(mainContainer, R.string.message_photo_download_error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLocationError() {
        Snackbar.make(mainContainer, R.string.message_location_error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showPhoto(Photo photo) {
        showPhotoDialog(PhotoDialog.newInstance(photo, imageSizeUri));
    }

    private void showPhotoDialog(PhotoDialog photoDialog){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag(PhotoDialog.TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        photoDialog.show(ft, PhotoDialog.TAG);
    }

    private static class PhotosAdapter extends RecyclerView.Adapter<PhotosViewHolder>{

        private final int imageSize;

        private final String imageSizeUri;

        private PhotoClickCallback clickCallback;

        private List<Photo> data;

        PhotosAdapter(int imageSize, String imageSizeUri, PhotoClickCallback clickCallback){
            this.imageSize = imageSize;
            this.imageSizeUri = imageSizeUri;
            this.clickCallback = clickCallback;
        }

        @Override
        public PhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_photo, parent, false);
            return new PhotosViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(PhotosViewHolder holder, int position) {
            final Photo photo = data.get(position);
            Uri uri = Uri.parse(photo.getPrefix()+imageSizeUri+photo.getSuffix());
            Picasso.with(holder.image.getContext()).load(uri).resize(imageSize, imageSize).centerCrop().noFade().into(holder.image);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickCallback.onPhotoClicked(photo);
                }
            });
        }

        @Override
        public int getItemCount() {
            return data != null ? data.size() : 0;
        }

        public void setData(List<Photo> data){
            this.data = data;
            notifyDataSetChanged();
        }

    }

    static class PhotosViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ip_image)
        ImageView image;

        PhotosViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    interface PhotoClickCallback{

        void onPhotoClicked(Photo photo);

    }

    public static class PhotoDialog extends DialogFragment{

        public static final String TAG = "photo.dialog";

        private static final String ARG_PHOTO = "arg.photo";

        private static final String ARG_IMAGE_SIZE_URI = "arg.image.size.uri";

        @BindView(R.id.dp_photo)
        ImageView imageView;

        private Photo photo;

        private String imageSizeUri;

        private Unbinder unbinder;

        public static PhotoDialog newInstance(Photo photo, String imageSizeUri){
            Bundle args = new Bundle();
            args.putParcelable(ARG_PHOTO, photo);
            args.putString(ARG_IMAGE_SIZE_URI, imageSizeUri);
            PhotoDialog res = new PhotoDialog();
            res.setArguments(args);
            return res;
        }

        public PhotoDialog(){}

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle args = getArguments();
            if(args != null){
                photo = args.getParcelable(ARG_PHOTO);
                imageSizeUri = args.getString(ARG_IMAGE_SIZE_URI);
            }
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            View res = inflater.inflate(R.layout.dialog_photo, container, false);
            unbinder = ButterKnife.bind(this, res);
            Uri uri = Uri.parse(photo.getPrefix()+imageSizeUri+photo.getSuffix());
            Picasso.with(res.getContext()).load(uri).noFade().into(imageView);
            if(getDialog() != null){
                getDialog().setCanceledOnTouchOutside(true);
            }
            return res;

        }

        @Override
        public void onDestroyView() {
            unbinder.unbind();
            super.onDestroyView();
        }
    }

}
