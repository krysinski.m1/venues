package m1.krysinski.foursquareimages.photos;

import java.util.List;

import m1.krysinski.foursquareimages.data.location.model.DeviceLocation;
import m1.krysinski.foursquareimages.data.location.source.LocationSource;
import m1.krysinski.foursquareimages.data.venues.model.Photo;
import m1.krysinski.foursquareimages.data.venues.source.PhotosSource;
import m1.krysinski.foursquareimages.permissions.LocationPermissionManager;

public class PhotosPresenter implements PhotosContract.Presenter, LocationSource.Callback {

    private LocationSource locationSource;

    private LocationPermissionManager locationPermissionManager;

    private PhotosSource photosSource;

    private PhotosContract.View view;

    public PhotosPresenter(LocationSource locationSource, LocationPermissionManager locationPermissionManager,
                           PhotosSource photosSource, PhotosContract.View view){
        this.locationSource = locationSource;
        this.locationSource.setCallback(this);
        this.locationPermissionManager = locationPermissionManager;
        this.photosSource = photosSource;
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void start() {
        loadPhotos();
    }

    @Override
    public void loadPhotos() {
        loadLocation();
    }

    @Override
    public void requestPermission() {
        locationPermissionManager.requestLocationPermission();
    }

    @Override
    public void openPhoto(Photo photo) {
        view.showPhoto(photo);
    }

    private void loadLocation(){
        view.showProgress();
        if(locationPermissionManager.hasLocationPermission()){
            locationSource.getCurrentLocation();
        }else{
            locationPermissionManager.requestLocationPermission();
        }
    }



    @Override
    public void onLocationAcquired(DeviceLocation location) {
        photosSource.getPhotos(location.getLatitude(), location.getLongitude(), new PhotosSource.GetPhotosCallback() {
            @Override
            public void onPhotosLoaded(List<Photo> photos) {
                view.showPhotos(photos);
                view.hideProgress();
            }

            @Override
            public void onError() {
                view.showPhotosError();
                view.hideProgress();
            }
        });
    }

    @Override
    public void onLocationError() {
        view.hideProgress();
        view.showLocationError();
    }
}
